$(document).ready(function(){
  $("nav li.haschild span").click(function(){
    $("nav li.haschild ul").hide();
    $(this).parent().find("ul").show();
  })
  $(".navicon").click(function(){
    $(this).toggleClass("navicontoggle");
    $("nav").toggleClass("opnnav");
    $("body").toggleClass("shift");
  })
    $('.slide-trusted-by').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 7,
        slidesToScroll: 1,
        autoplay: true,
        responsive: [
          {
            breakpoint: 1919,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 1365,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 1023,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
      $('.marquee').marquee({
        direction: 'down',
        duplicated: true,
        pauseOnHover: true
      });
      $('.marquee2').marquee({
        direction: 'up',
        duplicated: true,
        pauseOnHover: true
      });
})
